from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize("generate_slide_image_v2.py")
)