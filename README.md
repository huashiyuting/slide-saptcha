# 拖动验证器
    世面上的滑动验证器要么收费，要么就缺少维护不能使用，我寻找了一下午发现AJ-Captcha，tianai-captcha。但他们的写法要么部分开源，要么直接无人维护，要么就是写得很复杂。所以这边用65行代码来实现拖动验证器的背景图片和裁剪图片生成并记录位移率和位移量，喜欢的朋友可以star一下。V2源码开源，V2版采用了不规则图片验证及后期优化支持，谢绝白嫖~

## 所需环境
    python3.7.2
    
## 运行步骤
    1. pip3 install -r requirement.txt
    2. python generate_slide_image.py

## 前端滑动体验
    1. ./images/generate/index.html

## V2运行步骤
    1. pip3 install -r requirement.txt
    2. python generate_slide_img_v2_run.py

## 前端滑动体验
    1. ./images/generate/index_v2.html
## 截图
![输入图片说明](images/generate/%E6%97%A0%E6%A0%87%E9%A2%98.png)

## 在线测试链接
[在线测试链接](http://slide.zhuhui.store/)

## V2购买链接
[淘宝链接](https://item.taobao.com/item.htm?spm=a21dvs.23580594.0.0.6ffb645eKaCSCa&ft=t&id=725959026178)